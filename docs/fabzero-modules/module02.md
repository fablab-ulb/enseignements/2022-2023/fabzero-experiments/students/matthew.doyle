# 2. Conception Assistée par Ordinateur (CAO) 
 
 
Dans cette unité nous avons appris à utiliser un outil de design 3D, ce qu'est un 'compliant mechanism', et comment mettre différentes licences sur nos projets.  
 
 
## 2.1 Les outils de designs 3D 
 
 
Il existe de nombreux outils qui permettent de créer des modèles en 3D, mais je vais seulement expliquer comment j'ai utilisé deux d'entre eux, à savoir **FreeCAD** et **OpenSCAD**. 
 
 
 
### 2.1.1 FreeCAD  
 
 
FreeCAD est un logiciel qui permet de faire des sketchs en 2D et de les convertir en 3D. Pour le télécharger, il suffit de suivre [ce lien](https://www.freecad.org). En suivant les instructions dans les vidéos du [tutoriel](https://gitlab.com/fablab-ulb/enseignements/fabzero/cad/-/blob/main/FreeCAD.md), j'ai commencé par effectuer un sketch basique d'un rectangle : 
 
 
![](images/rectangle-sketch.jpg) 
 
 
J'ai alors converti ce rectangle en un modèle 3D : 
 
 
![](images/cube-freecad.jpg)  
 
 
Je n'ai pas vraiment aimé la façon de rendre les modèles paramétriques de FreeCAD, ou l'utilisation d'un spreadsheet est important. Je faisais aussi des petites erreurs dans l'utilisation du logiciel qui me faisaient perdre beaucoup de temps. J'ai donc décidé d'essayer d'utiliser OpenSCAD en priorité.  
 
 
### 2.1.2 OpenSCAD 
 
 
OpenSCAD est un logiciel qui permet de créer des modèles en 3D grâce à du code. Par exemple, pour créer un cube de côté 20, il suffit de taper dans la console : 
 
 
```
cube(20); 
``` 
 
 
J'ai ensuite appuyé sur le bouton qui permet de _Render_ le design, c'est à dire de l'afficher sur l'écran, et voici ce que j'ai obtenu : 
![cube](images/premier-essaie-openSCAD.jpg) 
 
 
Pour faire des modèles plus complexes, il est intéressant de connaître quelques opérations qu'on peut effectuer sur OpenSCAD. Certaines sont simples : 
 
 
``` 
Union(){  
shape 1; 
shape 2; 
} 
#Réunir deux formes géométriques 
 
 
difference(){ 
shape 1; 
shape 2; 
} 
#Faire la différence entre deux formes géométriques  
``` 
D'autres sont plus difficiles à comprendre : 
 
 
``` 
hull(){ 
shape1; 
shape2; 
} 
#créer la coque convexe entre 2 formes  
```  
Il y a encore beaucoup d'autres opérations qu'on peut effectuer sur OpenSCAD, mais les trois que je viens de montrer sont celles qui m'ont été les plus importantes dans la réalisation de ce module. Lorsque j'avais un souci avec une fonction ou je voulais tout simplement avoir plus d'information dessus, je consultais le [OpenSAD cheat sheet](https://openscad.org/cheatsheet/) et le [tutoriel sur Gitlab](https://gitlab.com/fablab-ulb/enseignements/fabzero/cad/-/blob/main/OpenSCAD.md). J'ai utilisé ces ressources pour effectuer mon premier design d'un objet en 3D : 
 
 
![](images/bridgeCAD.jpg) 
 
 
Mon prochain obstacle était que j'avais du mal à visualiser comment enchaîner plusieurs opérations complexes sur un même design. Pour m'aider dans ce but, j'ai regardé la documentation de [Pauline Mackelbert](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module02/), une étudiante ayant effectué ce cours l'année dernière, qui m'a montré qu'utiliser la fonction module permettait de rendre clair les différentes étapes de la création d'un objet : 
 
 
``` 
module name ( parameters ){ 
actions 
} 
``` 
L'outil _module_ permet essentiellement de créer une fonction qui effectue une action que nous pouvons programmer.  
Il est possible de créer des objets sans utiliser de module, mais c'est cette méthode qui m'a été la plus facile à comprendre.  
 
 
 
## 2.2 Compliant mechanisms 
 
 
Selon Wikipedia, un _compliant mechanism_ est un mécanisme flexible qui permet la transmission de force et de mouvement à travers la déformation élastique d'objets. Nous pouvons contrôler la fonction de ces objets en plaçant des contraintes précises sur les degrés de liberté des éléments flexibles. Les degrés de liberté d'un objet sont simplement les différents types de mouvements qu'il peut effectuer dans l'espace. Un objet totalement libre possède six degrés de liberté : 
 
 
* 3 de rotations  
* 3 de translations 
 
 
 
 
### 2.2.1 Design d'un FlexLink 
 
 
Un FlexLink est un type de _compliant mechanism_ simple. Mon idée a été de créer un FlexLink dont le design est similaire au premier design que j'ai effectué sur OpenSCAD:![](images/bridgeCAD.jpg) 
 
 
Je me suis dit que si le pont qui relie les deux blocs de cet objet était suffisamment mince, ceci permettrait de donner de la flexibilité au modèle. Je voulais aussi que les deux blocs puissent servir d'attache, pour que mon objet puisse être fixé à celui d'un autre élève. Pour réaliser ceci, il fallait que mon design soit paramétrique, pour que je puisse modifier les dimensions en fonction des modèles des autres élèves. Pour réaliser le design des attaches, je me suis inspiré du code de [Pauline Mackelbert](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module02/), que j'ai modifié à mes besoins. Voici le code que j'ai conçu :  
 
 
![](images/flexlink_fail.jpg) 
 
 
J'ai donc utilisé les paramètres suivants : 
``` 
hc #hauteur des cylindres composants l'attache  
rc #rayon des cylindres composants l'attache 
dc #distance entre les cylindres  
rc1 #rayon des trous dans l'attache  
da #distance entre les attaches  
bt #épaisseur du pont  
``` 
J'ai également utilisé une succession de modules pour construire l'objet : 
``` 
2cylindre #créer deux cylindres 
longcylinder #créer la forme de l'attache  
attache #créer les trous dans l'attache  
2attache #créer deux attaches  
 
 
``` 
J'ai ensuite réalisé qu'il y avait un problème avec ce modèle, car la longueur du pont reliant les attaches était trop grande, ce qui donnait ceci comme résultat : 
 
 
![](images/pursa-fail.jpg) 
 
 
J'ai donc modifié mon code de cette manière : 
 
 
![](images/Flexlink-first-version.jpg) 
 
 
En ajoutant un module en plus : 
 
 
``` 
try1 #créer le FlexLink complet  
``` 
 
 
 
 
 
## 2.3 Utilisation de licences  
 
 
Mettre une licence sur notre travail permet de contrôler comment ce travail peut être utilisé. Il est également important de citer toutes les personnes ayant contribué à la création du projet. Pour ce projet, j'ai choisi la licence CC BY, une licence creative commons qui permet à tout le monde d'utiliser le code tant que mon travail est cité par après. Étant donné que j'ai utilisé une partie du code de [Pauline Mackelbert](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module02/), j'étais obligé d'utilisé la licence CC BY (Car Pauline l'avait utilisé pour son projet) et de citer son travail. Tout ceci a été effectué en haut du document, de cette façon : 
 
 
``` 
File name: flexlink.scad  
 
 
date: 2023-03-01  
 
 
Author: Matthew Doyle  
 
 
License: Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) 
 
 
Adapted from: Flexlinks_module2.scad https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module02/ 
``` 
 
 
## 2.4 Version finale du code  
 
 
``` 


/*File name: flexlink.scad  

date: 2023-03-01 
 
 
Author: Matthew Doyle  
 
 
License: Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) 
 
 
Adapted from: Flexlinks_module2.scad https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module02/ 
//*/ 
 
 
hc = 8;  
rc = 2;  
dc = 4; 
rc1 =1; 
da = 40; 
bt = 0.5; 
$fn=200; 
module 2cylinder(height, distance, radius){ 
union(){ 
cylinder(h = height, r = radius, center = true); 
translate([distance, 0, 0])cylinder(h = height, r = radius, center = true); 
} 
} 
 
 
module longcylinder (height, distance, radius){ 
hull(){ 
2cylinder(height, distance, radius); 
} 
} 
module attache(height, distance, radius1, radius2){ 
difference(){ 
longcylinder(height, distance, radius1); 
2cylinder(height, distance, radius2); 
} 
} 
module 2attache(distance_attache,height,distance,radius1,radius2){ 
union(){ 
attache(height, distance, radius1, radius2); 
translate([0, distance_attache, 0])attache(height, distance, radius1, radius2); 
} 
} 
module try1(distance_attache,height,distance,radius1,radius2,band_thickness){ 
union(){ 
2attache(distance_attache,height,distance,radius1,radius2); 
translate([0,rc,-bt/2])cube([distance, distance_attache, band_thickness]); 
} 
} 
try1(da,hc,dc,rc,rc1,bt);

``` 
