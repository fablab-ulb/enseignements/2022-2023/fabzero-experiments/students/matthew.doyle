/*
File name: flexlink.scad 

#Author: Matthew Doyle 

#License: Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

#Adapted from: Flexlinks_module2.scad https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module02/
//*/
hc = 8; 
rc = 5.1; 
dc = 10.2;
rc1 =2.55;
da = 40;
bt = 1;
bl = 29.8;
$fn=100;
module 2cylinder(height, distance, radius){
    union(){
        cylinder(h = height, r = radius, center = true);
        translate([distance, 0, 0])cylinder(h = height, r = radius, center = true);
    }
}

module longcylinder (height, distance, radius){
    hull(){
        2cylinder(height, distance, radius);
    }
}
module attache(height, distance, radius1, radius2){
    difference(){
        longcylinder(height, distance, radius1);
        2cylinder(height, distance, radius2);
    }
}
module 2attache(distance_attache,height,distance,radius1,radius2){
   union(){
      attache(height, distance, radius1, radius2);
      translate([0, distance_attache, 0])attache(height, distance, radius1, radius2);
   }
  }
 module try1(longueur_bande,distance_attache,height,distance,radius1,radius2,band_thickness){
     union(){
         2attache(distance_attache,height,distance,radius1,radius2);
         translate([0,rc,-bt/2])cube([distance, longueur_bande, band_thickness]);
     }
 }
 try1(bl,da,hc,dc,rc,rc1,bt);

 
  

  
  
  