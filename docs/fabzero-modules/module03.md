# 3. Impression 3D 
 
 
Dans ce module nous avons appris comment utiliser une imprimante 3D pour imprimer un design effectué à l'aide d'un outil de CAD. Ceci nous a permis d'apprendre les bonnes pratiques lors de l'utilisation des imprimantes. Nous avons également formé des groupes dans lesquels nous avons coordonné nos paramètres de design pour créer des objets complexes.  
 
 
 
 
 
## 3.1 Installation du slicing software  
 
 
La première étape a été d'installer un logiciel qui permet d'envoyer nos modèles à l'imprimante 3D, ainsi que de contrôler les paramètres d'impression. Pour cela, nous avons utilisé [PrusaSlicer](https://help.prusa3d.com/en/article/install-prusaslicer_1903). Avant d'envoyer le modèle au logiciel PrusaSlicer, il faut convertir le fichier en STL, pour que PrusaSlicer puisse le reconnaître.  
 
 
![](images/STL-render.jpg)  
 
 
Nous étions alors prêts à ouvrir l'application PrusaSlicer. Lors de la première ouverture, nous avons mis l'application en mode avancé, comme il était conseiller dans le [tutoriel](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md) FabLab.  
Pour ajouter un fichier dans PrusaSlicer, il faut alors simplement appuyer sur l'icône suivante : 
 
 
![](images/pursa-slicer1.jpg)  
 
 
 
## 3.2 Préparation à l'impression  
 
 
### 3.2.1 Réglages des paramètres avant l'impression  
 
 
Il y a plusieurs paramètres qu'il faut modifier sur PrusaSlicer: 
 
 
* Il faut s'assurer que le bon modèle d'imprimante est sélectionné. Le modèle des imprimantes que nous avons utilisées est Original Prusa i3 MK3S & MK3S+ à buse de 0,4mm, que l'on peut retrouver dans les imprimantes pré-enregistrées.  
 
 
* Il faut sélectionner le type de filament (le type de plastique que l'imprimante 3D utilise pour construire l'objet) que l'on va utiliser. Pour mes impressions, le paramètre 'generic PLA' a très bien fonctionné.  
 
 
* Il faut sélectionner la hauteur des couches imprimées, que j'ai fixé à 0,20mm. 
 
 
* Il faut sélectionner le pourcentage de remplissage, que j'ai fixé à 20% après un essai à 15%. 
 
 
* Il faut sélectionner l'épaisseur des parois, que j'ai mis sur 3 périmètres, pour m'assurer que les parois soient solides sans trop augmenter le temps d'impression.  
 
 
Il y a aussi certaines structures qui permettent de s'assurer d'une bonne impression que l'on peut ajouter à son design, à savoir : 
 
 
* La bordure, qui permet d'augmenter l'adhérence au plateau. Celle-ci n'est utile que si l'objet imprimé est très grand ou si la surface de contact avec le plateau est petite. Comme je considérais que mon design était dans le deuxième cas, j'ai décidé d'en ajouter une. Ma pièce était plutôt petite, il n'y avait donc pas de chance que la bordure recouvre la jupe, qui est un filament imprimé automatiquement par l'imprimante pour tester l'adhérence au plateau.  
 
 
* Les supports, qui peuvent être nécessaires lorsqu'on veut imprimer des structures dans le vide, ou des éléments avec un angle de surplomb supérieur à 45°. Pour voir les éléments qui nécessitent des supports dans notre design, on peut appuyer sur le bouton 'slice now' en bas à droite, et les parties qui nécessitent du support deviennent bleues : 
 
 
![](images/support.jpg)  
 
 
Tous ces paramètres peuvent être modifiés sur l'écran d'accueil : 
 
 
![](images/prusa-parameters.jpg) 
 
 
L'onglet 'réglages d'impressions' permet également de faire des modifications plus en détails: 
![](images/prusa-parameters2.jpg) 
 
 
### 3.2.2 Dernières étapes avant l'impression  
 
 
Pour déverrouiller les imprimantes 3D, il suffit de scanner le code QR qui se trouve sur la machine et introduire son identifiant GitLab. Nous avons ensuite été sur PrusaSlicer pour appuyer sur les boutons ***Slice now*** et puis ***Export G-code***. Le logiciel a alors créé un fichier contenant le code nécessaire à l'impression, qu'il faut sauvegarder sur une clé SD qui peut être introduite dans l'imprimante. Avant chaque impression, nous avons nettoyé les plaques pour nous assurer qu'il n'y avait pas d'imperfection qui pourrait causer une mauvaise adhérence. 
 
 
## 3.3 Impressions et optimisations  
 
 
J'ai utilisé le design de mon FlexLink que j'ai effectué dans le [module 2](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/matthew.doyle/fabzero-modules/module02/) comme un torture test. Un torture test est un modèle que l'on imprime pour tester les capacités de notre imprimante 3D. Je voulais tout d'abords voir à quel point le FlexLink que j'avais conçu serait flexible, tout en m'assurant qu'il ne serait pas trop cassant. J'ai donc commencé par imprimer ce modèle : 
 
 
![](images/Flexlink-first-version.jpg) 
 
 
Après 13minutes d'attente, j'ai obtenu l'objet suivant :  
 
 
![](images/flexlink1-3D.jpg)![](images/flexlink1-3Dprint.jpg) 
 
 
 
Le FlexLink était très flexible, mais également très petit. De plus, le pont reliant les attaches était très mince, et je pensais donc qu'il ne serait pas très résistant. J'ai donc décidé de modifier les paramètres de mon design, en augmentant l'épaisseur du pont et en le rendant un peu moins long et plus large. J'ai également augmenté la taille des attaches, de sorte à ce que mon FlexLink puisse être utilisé par d'autres élèves. [Lorea Latorre](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/lorea.latorre/) et moi avons alors formé un groupe, car son modèle possédait des pièces qui pourraient s'attacher au mien, comme vous pouvez le constater : 
 
 
![](images/flexlink-Lorea.jpg) 
 
 
Nous avons tous les deux dû adapter notre code, pour que les embouts dans son modèle puissent s'emboîter dans les attaches de mon design. J'ai donc dû modifier la taille de mes attaches, ainsi que le rayon des trous cylindriques dans celles-ci. Pour déterminer les dimensions des embouts et des trous, j'ai demandé à un groupe d'élèves qui avait déjà effectué un test dans ce but. Leur méthode était d'imprimer une pièce avec plusieurs trous de rayons différents, et de voir les dimensions des embouts qui pouvaient se fixer dans les trous. Nous avons donc décidé de donner un rayon de 2,45mm aux embouts et 2,55mm aux trous. J'ai donc effectué les modifications suivantes à mon code : 
 
 
 
``` 
 
 
/* 
File name: flexlink.scad  
 
date: 2023-03-01 
 
#Author: Matthew Doyle  
 
 
#License: Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) 
 
 
#Adapted from: Flexlinks_module2.scad https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module02/ 
//*/ 
hc = 8;  
rc = 5.1;  
dc = 10.2; 
rc1 =2.55; 
da = 40; 
bt = 1; 
bl = 29.8; 
$fn=200; 
module 2cylinder(height, distance, radius){ 
union(){ 
cylinder(h = height, r = radius, center = true); 
translate([distance, 0, 0])cylinder(h = height, r = radius, center = true); 
} 
} 
 
 
module longcylinder (height, distance, radius){ 
hull(){ 
2cylinder(height, distance, radius); 
} 
} 
module attache(height, distance, radius1, radius2){ 
difference(){ 
longcylinder(height, distance, radius1); 
2cylinder(height, distance, radius2); 
} 
} 
module 2attache(distance_attache,height,distance,radius1,radius2){ 
union(){ 
attache(height, distance, radius1, radius2); 
translate([0, distance_attache, 0])attache(height, distance, radius1, radius2); 
} 
} 
module try1(longueur_bande,distance_attache,height,distance,radius1,radius2,band_thickness){ 
union(){ 
2attache(distance_attache,height,distance,radius1,radius2); 
translate([0,rc,-bt/2])cube([distance, longueur_bande, band_thickness]); 
} 
} 
try1(bl,da,hc,dc,rc,rc1,bt); 
 
 
```  
J'ai ensuite exporté ce code en format STL, et je l'ai ouvert sur PrusaSlicer, où j'ai utilisé tous les paramètres que j'ai expliqué précédemment, en augmentant le pourcentage de remplissage à 20%, pour être sûr que mon objet serait solide.  
 
 
La version finale du code de Lorea était comme suit : 
 
 
![](images/lorea-code.jpg)  
 
 
Voici la version finale de mon FlexLink:  
 
 
![](images/flexlink2-3D-apple.jpg) 
 
 
Cette version est bien plus résistante et grande que la première, tout en gardant beaucoup de flexibilité: 
 
 
![](images/flexlink-bent.jpg) 
 
 
Mon Flexlink était aussi capable de s'attacher à l'autre :  
 
 
![](images/2flexlinks.jpg) 
 
 
Nous avons donc finalement obtenu un objet avec deux parties capables de se fixer l'un à l'autre de beaucoup de manières différentes. Comme il n'y a que 1 point d'attache, les deux parties peuvent faire des rotations autour de l'axe du point de liaison, et d'autres modèles ayants les mêmes dimensions pour les trous ou les embouts peuvent également se liés à notre objet. Les deux parties sont également flexibles, ce qui pourrait permettre certaines fonctions que nous n'avons pas encore imaginé.    

