# 5. Dynamique de groupe et projet final 
 
 
## 5.1 Analyse et modélisation de projet ([Le Planktoscope](https://www.planktoscope.org)) 
### Arbre de problèmes : 
 
 
![](images/problem-tree.jpg) 
 
 
Le plancton est absolument capital à tous les écosystèmes marins, mais peu d'information est connue sur les différents types et le mouvement de ces micro-organismes. Il y a donc peu d'information sur l'impact des activités humaine sur le plancton, et il est donc difficile de mettre en place des stratégies de conservation. Cette pénurie d'information est principalement dû au manque d'équipement et de bateaux spécialisés, ainsi qu'aux surfaces devant être étudié qui sont trop vastes.  
 
 
 
### Arbre de solutions : 
 
 
![](images/solution-tree.jpg) 
 
 
Pour réussir à collecter plus d'information sur le plancton, le Planktoscope a été développé. C'est un outil simple à l'utilisation qui permet d'obtenir une grande quantité d'images d'un échantillon de plancton. Le modèle du Planktoscope est disponible en ligne, et il y a déjà une grande communauté de personnes et d'organisations qui l'utilise. Cet outil devrait permettre d'obtenir plus d'informations sur les écosystèmes marins et sur l'impact des activités humaines sur ces écosystèmes. Il devrait également faciliter la mise en place de stratégies de conservations.  
 
 
## 5.2 Formation de groupe et discussion sur le projet finale 
 
 
### 5.2.1 L'objet que j'ai pris avec moi et les thématiques qu'il représente 
 
 
Cette semaine, nous avons été demandés de prendre un objet avec nous qui représente un thème ou un problème sociétal qui est important à nos yeux. J'ai choisi de prendre avec moi un couvercle de yaourt au soja, car il représente plusieurs problèmes que je trouve pressant :  
 
 
* La déforestation causée par l'agriculture  
 
 
* La pollution des sources d'eau par l'utilisation de pesticides et d'engrais  
 
 
* La perte de la fertilité des sols par l'utilisation de pesticides et d'engrais  
 
 
* La surutilisation de produits importées par rapport à ceux produits localement.  
 
 
### 5.2.2 La formation des groupes 
 
 
Pour former Les groupes, nous nous sommes tout d'abord disposés en cercle pour dire ce qu'est notre objet. Nous avons ensuite été dans la direction de personnes que nous pensions avaient un objet similaire au notre. Nous avons ensuite formé des groupes de 4 ou 5 personnes avec les étudiants ayant l'objet le plus semblable au notre. J'étais d'abord dans un groupe avec des personnes qui avaient pris des objets qui représentais globalement des problèmes dans le recyclage. Ce n'est pas nécessairement le sujet qui m'intéresse le plus, et il y avait trop d'élèves dans le groupe, je me suis donc dirigé vers un groupe où l'accent était plutôt sur l'impact environnemental du transport. Je trouve ce thème plus intéressant et en lien avec mon objet. Voici les objets que toutes les personnes du groupe ont amené :  
 
 
![](images/objets-groupe.jpg) 
 
 
 
Nous nous sommes alors présentés et avons expliqué en quelques mots les problèmes que représentent nos objets.  
Dans notre groupe, il y avait trois personnes ayant pris des objets en lien avec la pollution liée au transport: [Ayman Taifour](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/aymane.taifour/), qui a pris un ticket de train, [Virginie Louckx](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/virginie.louckx/), qui a pris des clés de voiture, [Mariam Mekray](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/mariam.mekray/), qui a pris une voiture de jouet. Il y avait ensuite [Lorea LATORRE MOLINA](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/lorea.latorre/), qui a pris une pièce de monnaie, représentant le remplacement de l'argent liquide par l'argent digitale.  
 
 
### 5.2.3 La thématique commune qui ressort de nos objets 
 
 
Pour essayer de dégager une thématique commune des objets que nous avons apportés, nous avons tous écrit sus des post-it's des thèmes sur lesquelles nous aimerions travailler qui étaient en lien avec les objets que nous avons pris. Voici le résultat :  
 
 
![](images/thematique-postit's.jpg)  
 
 
Nous avons alors été demandé de classer individuellement les différentes thématiques en fonction de si on était :  
 
 
* Très enthousiastes de travailler sur ce thème 
* D'accord de travailler sur ce thème 
* D'accord de suivre le groupe sur ce thème  
* Pas d'accord de travailler sur ce thème  
 
 
Voici le résultat de cet exercice : 
 
 
![](images/thematique-ranking.jpg) 
 
 
La thématique 3 'la pollution liée au transport, écrite par **Mariam**, a donc été choisi (provisoirement).  
 
 
### 5.2.4 Le début du choix d'une problématique 
 
 
Pour choisir une problématique, il nous a été conseillé de commencer chaque phrase par "moi à ta place, je..." pour nous encourager de rebondir sur les idées des autres dans le groupe. Je trouve personnellement que cette technique limite la conversation d'une façon qui n'est pas nécessairement bénéfique. Les idées qui sont ressorties de cet exercice ont été retranscrites par **Mariam**: 
 
 
![](images/problematique.jpg) 
 
 
Nous avons ensuite envoyé **Virginie** pour demander dans les autres groupes des idées de problématiques liées à notre thème :  
 
 
![](images/problematique-autres-groupes.jpg) 
 
 
Les autres groupes ont également désignés des portes paroles qui sont passés dans notre groupe pour demander des idées de problématique.  
 
 
### 5.3 Dynamique de groupe 
 
 
Après avoir formé nos groupes de projet final, nous avons appris quelques techniques pour nous aider à mieux travailler en groupe. Je vais en présenter 3 que je pense vont nous être utiles : 
 
 
***1.Division des rôles de chaque réunion*** 
 
 
Pour chaque réunion du groupe, il est intéressant de désigner : 
 
 
* Un animateur, qui s'assure que toutes les personnes ont l'opportunité de parler, et que la personne qui prend la parole ne dit pas des choses que le reste du groupe n'a pas besoin de savoir. 
 
 
* Une personne en charge de la gestion du temps, qui s'assure que le groupe consacre une quantité de temps raisonnable à chaque problème  
 
 
* Un secrétaire, qui se charge de prendre des notes des points les plus importants.  
 
 
***2.L'art de décidé*** 
 
 
Globalement, il y a trois façons différentes de prendre une décision : 
 
 
* Collectivement, si tous les membres du groupe devraient donner leurs avis.  
 
 
* Par une personne, si une personne possède plus de compétences dans ce domaine.  
 
 
* Elle peut ne pas être prise, si le moment n'est pas opportun 
 
 
***3. Méthodes de décisions*** 
 
 
Plusieurs méthodes de prise de décisions nous ont été présentés :  
 
 
* La prise de décision par vote majoritaire, pour des décisions importantes.  
 
 
* Les votes pondérés et classés, qui permettent aux différentes personnes dans le groupe d'exprimer leur niveau d'enthousiasme pour chaque option disponible.  
 
 
* Le tirage au sort, pour prendre des décisions rapides.