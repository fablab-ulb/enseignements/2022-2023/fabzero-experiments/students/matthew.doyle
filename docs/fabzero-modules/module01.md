# 1. Gestion de projet et documentation 
 
 
Dans cette première partie du cours, nous avons appris à utiliser un command line user interface (CLI) avec le langage BASH, documenter notre travail pendant sa réalisation, et utiliser des principes de gestion de projet.  
 
 
## 1.1 Comment utiliser un CLI avec BASH  
 
 
Avec un PC MacBook, la façon la plus simple d'utiliser un CLI est d'ouvrir le terminal. Le message suivant s'affiche alors : 
``` 
The default interactive shell is now zsh. 
To update your account to use zsh, please run `chsh -s /bin/zsh`. 
For more details, please visit https://support.apple.com/kb/HT208050. 
Matthews-MacBook-Air:~ matthewdoyle$  
``` 
 
 
Le premier message suggère une mise à jour pour pouvoir utiliser zsh comme shell unix. Un shell unix est simplement un interpréteur de commande que l'on introduit dans le terminal. Le shell suggéré pour ce cours est **BASH** (bourne again shell), qui est utilisé par défaut sur Mac, il n'est donc pas nécessaire d’effectué cette mise à jour. Pour s'assurer que le terminal utilise bien BASH, on peut utiliser la commande suivante : 
 
 
``` 
echo "$SHELL" 
``` 
 
 
**echo** est une commande BASH dont l'output est le texte produit par la commande qui lui a été introduite. Il existe beaucoup de commande BASH comme celle-ci qui sont utile à connaître. J'ai utilisé le [tutoriel FabZero](https://github.com/Academany/fabzero/blob/master/program/basic/commandline.md) et le [tutoriel ubuntu](https://ubuntu.com/tutorials/command-line-for-beginners#1-overview) pour apprendre quelques commandes basiques de BASH: 
 
 
 
``` 
mkdir # créer un 'directory'(fichier)  
 
 
cd #changer de directory  
 
 
pwd #print working directory (dire dans quel fichier on se trouve) 
 
 
ls #créer une liste  
 
 
man #obtenir de l'information sur une commande 
``` 
 
 
 
## 1.2 Documentation  
Le but principal du premier module a été de nous apprendre à documenter notre travail au fur et à mesure de sa réalisation.  
 
 
Dans ce but, nous avons été invités à écrire cette documentation et de la poster sur notre site web personnel grâce à l'outil GitLab. 
 
 
Cette partie du premier module a été la plus difficile pour moi à comprendre, car je possède à ce stade des compétences très limitées en informatique, et je n'ai jamais utilisé GitLab auparavant.  
 
 
### 1.2.1 Utilisation de Markdown dans un éditeur de texte 
 
 
Pour écrire la documentation, nous avons utilisé un éditeur de texte. J'ai choisi d'utiliser [Visual Studio Code](https://code.visualstudio.com) car il est open source et possède une fonction preview qui permet de voir à quoi ressembleront les fichiers écrits en Markdown.  
 
 
Une fois Visual Studio Code téléchargé, j'ai téléchargé quelques extensions sur l'application : 
 
 
![extensions](images/extensions-vsiualstudiocode.jpg) 
 
 
L'extension la plus utile pour moi a été le Code Spell Checker, qui permet de corriger les fautes d'orthographes dans Visual Studio Code. Une autre extension utile est le _Embed images in Markdown_, qui permet de placer des images dans les documents sans devoir écrire les liens, simplement en les plaçant dans le document avec la souris. En effet, l'extension crée automatiquement un lien et une référence pour l'image dans le document, mais cette méthode prend beaucoup de place dans un document. C'est donc généralement mieux d'écrire le lien soi-même.  
 
 
**Markdown** est une version simplifiée de HTML qui permet d'écrire des scriptes pour des sites internet. Pour apprendre à utiliser Markdown, j'ai suivi le [tutoriel FabZero](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#writing-documentation-in-markdown) qui m'a mené à ce [tutoriel](https://www.markdowntutorial.com) qui utilise des exemples et des exercices pour faciliter l'apprentissage. Voici quelques commandes qui m'ont été utiles lors de la réalisation de la documentation : 
 
 
``` 
# #titre  
## #sous titre  
** ** #écrire un mot en gras  
_ _ #écrire un mot en italique  
[texte](lien) #ajouter un lien à du texte  
![caption](lien) #ajouter une image 
* #créer une liste  
 
 
``` ``` #écrire du code  
``` 
### 1.2.1 Modification de photos  
 
 
Avant d'insérer des photos ou des screenshots, il est intéressant de les modifier pour diminuer l'espace de stockage qu'elles demandent. J'ai choisi d'utiliser [Gimp](https://www.gimp.org) dans ce but. Une fois une image ouverte avec Gimp, nous pouvons sélectionner _image_, puis _scale_, et modifier la taille et la resolution en X et en Y : 
 
 
![](images/scale.jpg)  
 
 
Modifier ces deux paramètres permet de diminuer considérablement la taille des images (en bytes), en particulier la résolution en X et Y, qui est généralement beaucoup trop grande pour la plupart des images qui ont été prisent avec un téléphone par exemple.  
 
 
### 1.2.2 Utilisation de Git et GitLab 
 
 
### 1.2.2.1 Première modification de mon site personnel  
 
 
Gitlab est une plateforme en ligne qui permet de garder des fichiers qui peuvent être modifié par plusieurs personnes. Les fichiers peuvent également servir à créer des sites internet, comme ça a été le cas pour celui-ci. Les fichiers peuvent être modifiés directement sur Gitlab, et c'est de cette façon que j'ai effectué ma première modification de mon site personnel : 
 
 
![](images/first-edit.jpg) 
 
 
Pour accéder à ce fichier il faut aller à [l'espace dédié à notre classe sur Gitlab](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments), et sélectionner _students_, puis son _nom_, puis _docs_, et finalement le fichier qui s'appelle _index.md_. Il faut alors cliquer sur l'option _Edit_, ce qui fait apparaître le code que l'on voit sur l'image ci-dessus.  
 
 
#### 1.2.2.2 Comment installer et configurer Git 
 
 
Pour cloner et éditer localement les fichiers Gitlab, il faut utiliser Git. Pour télécharger Git, j'ai utilisé [homebrew](https://brew.sh), que j'ai téléchargé en suivant les instructions sur leur site : 
 
 
![](images/homebrew-install.jpg) 
 
 
La procédure pour installer Git est alors très simple, comme vous pouvez le constater : 
 
 
![](images/git-instalation.jpg)  
 
 
Nous avons ensuite dû configurer Git avec **le même username et email que ceux sur notre compte GitLab**: 
 
 
``` 
git config --global user.name "your_username" 
git config --global user.email "your_email_address@example.com" 
``` 
Si ces paramètres ne sont pas correctement introduits, git ne reconnaîtra pas le compte sur GitLab que nous essayons de modifier. 
 
 
#### 1.2.2.2 Comment cloner et éditer un projet de gitlab  
 
 
Pour cloner un projet GitLab, j'ai choisi d'utiliser une clé ssh. Il faut tout d'abords créer une clé en utilisant la commande :  
``` 
ssh-keygen -t ed25519 -C "<comment>"  
``` 
 
 
Nous avons ensuite copié la clé publique, puis ouvert l'onglet qui permet de modifier son profil et sélectionner SSH Keys pour ajouter la clé à son compte Gitlab. Sur mac les fichiers .ssh sont normalement cachés, il faut donc utiliser cmd+shift+G lorsque le finder est ouvert pour trouver la clé publique qui a été générée.  
 
 
Nous avons ensuite cloné le dépôt de fichiers grâce à l'ajout de cette clé sur GitLab, et créé une branche de ce dépôt qu'on peut éditer, grâce aux commandes suivantes :  
 
 
``` 
git clone 'adresse du repositoire' 
git checkout -b <name-of-branch> 
``` 
Les fichiers sont alors disponibles sur l'ordinateur sur lequel nous avons effectué les commandes. Nous avons alors pu modifier les fichiers en les ouvrant dans un éditeur de texte. 
Pour envoyer les changements effectués localement sur GitLab, il faut suivre les commandes suivantes : 
 
 
```  
git add <file-name OR folder-name> 
 
 
git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT" 
 
 
git push <remote> <name-of-branch> 
 
 
``` 
 
 
Voici un exemple de code ou j'ai effectué ceci : 
 
 
![](images/editing-gitlab-folders2.jpg) 
 
 
Une fois que toutes ces étapes sont accomplies, la branche que l'on vient d'envoyer se retrouve sur GitLab. Il faut alors fusionner cette branche à la branche principale pour que les changements soient enregistrés sur la branche principale, qui sert de base pour le site personnel.  
 
 
#### 1.2.2.3 Utilisation de MkDocs 
 
 
MkDocs est l'outil qui permet de convertir les fichiers markdown en sites internet. Pour le télécharger, il faut avoir des versions à jour de python et pip installées sur son ordinateur. Pour télécharger cet outil, il suffit alors d'utiliser la commande suivante : 
 
 
``` 
pip3 install mkdocs 
``` 
 
 
J'ai choisi de télécharger MkDocs car il m'a permis de voir les changements que j'ai effectué sur mon site sans devoir attendre qu'il soit mis à jour, en me plaçant dans le directory qui contient mes fichiers markdown, et en utilisant la commande : 
 
 
``` 
mkdocs serve 
``` 
Cette commande crée alors un lien vers un serveur locale avec une version du site personnel que nous avons créé sur notre branche. 
 
 
 
## 1.3 Gestion de projet  
 
 
Ce module a également servi à une initiation aux concepts de gestion de projet. Ces concepts sont difficiles à implémenter, mais peuvent être utiles à connaître pour certaines personnes. La majorité des concepts j'ai appris se trouve [içi](http://fablabkamakura.fabcloud.io/FabAcademy/support-documents/projMgmt/#as-you-work-documentation).  
 
 
Il y a certains concepts que j'avais déjà l'habitude d'implémenter, comme le 'spiral development'. Commencer par compléter une version basique d'un projet, pour ensuite ajouter de la complexité, a toujours été ma façon de travailler. J'ai également l'habitude d'organiser mon travail en fonction du temps auquel je peux y consacrer, et de résoudre les projets en utilisant un 'bottom-up Debugging' (je me demande même comment il serait possible de faire autrement). Le 'triage', 'Modular & Hierarchical Planning' et le 'Parallel Development' sont toutes des techniques qui m'ont permis de travailler de façon plus efficace, mais que je trouve difficiles à implémenter de façon régulière. Finalement, je n'ai pas réussi à effectuer la documentation des projets au fur et à mesure de leurs réalisations. Je me suis contenté de faire des Screenshots et d'écrire quelques notes à des moments cruciaux, car le procédé pour modifier mon site personnel a été très difficile à apprendre pour moi, même s'il pourrait sembler trivial pour certains. 
      