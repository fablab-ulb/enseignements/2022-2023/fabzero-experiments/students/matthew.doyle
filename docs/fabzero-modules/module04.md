# 4. Outil FabLab 
 
 
Cette semaine nous avons pu choisir entre quatre formations différentes sur des outils disponibles au FabLab. J'ai choisi de suivre la formation sur la fraiseuse CNC (Computerized Numerical Control), qui permet de faire des découpes très précisent d'objets complexes dans des matériaux variés (bois, certains métaux). 
 
 
## 4.1 Installation de Fusion 360  
 
 
L'outil de design que nous avons utilisé est Fusion 360. Il est possible de télécharger une version gratuite du logiciel lorsqu'on est étudiant grâce à [ce lien](https://www.autodesk.com/education/edu-software/overview?sorting=featured&filters=individual). Malheureusement, pour des raisons qui ne sont pas tout à fait claires, cela ne fonctionnait pas pour moi, j'ai donc dû utiliser l’essai gratuit de 1 mois, que l'on peut retrouver grâce au lien plus haut.  
 
 
 
 
## 4.2 Premiers pas dans fusion 360  
 
 
Pour nous préparer à utiliser fusion 360 pour construire notre propre objet, on nous a donné un fichier contenant le design d'un objet déjà fait, que nous avons ouvert sur fusion 360 : 
 
 
![](images/premier-design.jpg) 
 
 
Nous avons alors été dans l'onglet **manufacture** pour l'objet en question : 
 
 
![](images/manufacture.jpg)  
 
 
Pour que la fraiseuse comprenne comment construire l'objet, il faut rentrer les bons paramètres pour chaque action qu'on veut qu'elle entreprenne. Pour cela il faut commencer par appuyer sur l'onglet **setup**, juste à côté de l'onglet où on choisit si on design ou manufacture (voir la première photo). 
 
 
## 4.2.1 Le brute et les axes  
 
 
La première étape dans la configuration des paramètres est de placer les axes et configurer le brute. Le brute est tout simplement le matériau dans lequel la fraiseuse va couper. Si on utilise par exemple une planche en bois il faut encoder les dimensions exactes de cette planche. Fusion 360 est en anglais sur mon ordinateur, et le mot brute est donc remplacé par 'Stock'. L'onglet où l'on peut modifier les axes et le brute apparaît automatiquement lorsqu'on ouvre un nouveau set-up: 
 
 
![](images/setup1.jpg) 
 
 
Dans cet onglet il faut sélectionner une des trois options qui permettent de sélectionner manuellement les axes dans 'Orientation' dans l'onglet **Work Coordinate system**. Lorsque cette option est sélectionnée, il suffit de sélectionner une arrête dans le design qui est dans le même sens que l'on désire pour notre axe. Pour sélectionner l'origine pour nos axes, il faut sélectionner 'Stock box point' dans 'Origin', et il est préférable de sélectionner un point à la surface supérieur du modèle qui est au centre de celui-ci.  
 
 
Ensuite dans l'onglet Stock, on peut rentrer les dimensions précises de notre brute lorsqu'on sélectionne 'fixed sized box' dans 'mode' dans le sous-onglet **stock**, comme vous pouvez le voir :  
 
 
![](images/stock-settings.jpg) 
 
 
## 4.2.2 Configuration de l'outil 
 
 
Pour que la fraiseuse fonctionne correctement, il a fallu rentrer les paramètres exacts de l'outil que nous avons utilisé. Pour cela il faut sélectionner 'edit tool', puis 'new tool', et encoder les paramètres exacts de notre outil. 
 
 
## 4.2.3 Poche 2D, ébauche adaptative 2D, rainure et contour.  
 
 
Chaque découpe de la fraiseuse doit être correctement paramétré. Pour accomplir ceci, il faut utiliser cet onglet :  
 
 
![](images/2Ddesign.jpg)  
 
 
 
Pour fabriquer mon objet final, j'ai utilisé les options 2D Pocket, 2D Adaptive Clearing, 2D Contour et Slot. Voici les paramètres qu'il faut modifier pour chacun : 
 
 
**-2D Pocket** 
 
 
Il faut d'abords aller dans le second onglet, et appuyer sur 'pocket selections', puis sélectionner le fond des poches que l'on veut modifier, comme ceci : 
 
 
![](images/pocket.jpg) 
 
 
Ensuite il faut sélectionner le quatrième onglet, et cocher 'Multiple Depths' et décocher 'Stock to leave' :  
 
 
![](images/pocket-depths.jpg) 
 
 
Dans 'Multiple Depths', il faut s'assurer que le Maximum Roughing Stepdown à une valeur égale à la moitié de la longueur de l'outil utilisé.  
 
 
Dans le deuxième onglet, il faut aussi s'assurer que le paramètre bottom height permet bien à la fraiseuse de passer à travers le brute.  
 
 
**-2D Adaptive clearing** 
 
 
Cet outil permet de créer des poches de formes variées. Les paramètres qu'il faut modifier sont identiques à ceux pour le 2D Pocket.  
 
 
**-2D Contour**  
 
 
Les paramètres qu'il faut modifier sont globalement identiques à ceux pour le 2D Pocket, sauf qu'il faut s'assurer d'ajouter des attaches pour que notre objet soit maintenu en place, ce qu'on peut faire dans le deuxième onglet, comme ceci : 
 
 
![](images/tabs.jpg) 
 
 
 
**-2D Slot**  
 
 
Cet outil permet de faire des rainures dans notre objet. Les paramètres sont globalement modifiés de façon identique aux paramètres précédents.  
 
 
 
 
 
 
## 4.3 Design d'un premier objet  
 
 
Pour faire le design de notre objet, nous aurions pu utiliser un des outils de CAD que j'ai présenté au [module 2](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/matthew.doyle/fabzero-modules/module02/), mais j'ai choisi d'utilisé fusion360 par soucis de facilité. J'ai choisi de faire un domino, car je voulais l'utiliser comme décoration. Pour faire le design du domino, j'ai tout d'abords fait un sketch d'un rectangle, que j'ai extrudé.  
 
 
![](images/design-tools.jpg)  
 
 
J'ai ensuite utilisé la fonction fillet pour donner des coins arrondis à ma structure, et créer des trous et un autre rectangle que j'ai également extrudé pour donner la forme du domino :  
 
 
![](images/domino-design-1.jpg) 
 
 
J'ai ensuite été dans l'onglet manufacture, et j'ai effectué les modifications comme je les ai expliqué au point 4.2.3 
 
 
 
### 4.3.1 Les problèmes du premier design  
 
 
Il y avait quelques problèmes avec mon premier design : 
 
 
- Je n'avais pas bien encodé la taille de mon brute, et j'ai donc dû modifier les paramètres de mon objet pour qu'il ait la même épaisseur que mon brute, car sinon la découpe aurait prise trop de temps. 
- Comme mon domino était plus épais, j'ai également dû rendre les trous et rainures plus profonds dans l'objet, et tenir compte du diamètre de l'outil que nous avons utilisé (l'outil avait un diamètre de 4mm, il fallait donc par exemple que les trous du domino soient plus grands pour que la fraiseuse puisse fonctionner) 
- Pour toutes mes modifications (poches 2D, rainures etc.), il fallait que je modifie, dans la partie **Ramp** du dernier onglet, le 'Ramp type' pour qu'il soit hélicoïdale et le 'Helical Ramp Diameter' pour qu'il soit égale à 1mm, car sinon la fraiseuse n'aurait pas pu faire des trous de petits diamètres.  
 
 
Voici mon domino après les modifications : 
 
 
![](images/domino-design2.jpg) 
 
 
### 4.3.2 Code  
 
 
Pour générer un code que la fraiseuse pourra comprendre, il faut appuyer sur actions, puis post process :  
 
 
![](images/post-process.jpg)  
 
 
Nous avons alors sauvegardé le code généré sur une clé USB, que l'on peut alors insérer dans l'ordinateur contrôlant la fraiseuse.  
 
 
 
## 4.4 Manufacture de l'objet  
 
 
Avant de commencer la découpe, il faut s'assurer que notre brute ne bouge pas pendant la découpe, en la maintenant avec des vices. Il faut alors dessiner une croix au milieu de notre brute, pour nous montrer où devrait se trouver l'origine de nos axes. Il faut alors placer précisément l'outil de la fraiseuse au-dessus de notre croix. Pour les axes X et Y, cela peut se faire à l'œil, mais pour l'axe Z on utilise un objet d'hauteur connue par la machine, pour s'assurer qu'on n'endommage pas le brute. Nous avons alors commencé la découpe. Il faut toujours s'assurer d'être attentif durant tout le processus de découpage, et de porter des protèges oreilles car c'est une machine qui fait un bruit assourdissant.  
 
 
Voici le résultat : 
 
 
![](images/decoupe.jpg) 
 
 
J'ai ensuite enlevé les attaches, et j'ai utilisé du papier de verre pour rendre l'article fini un peu plus propre :  
 
 
![](images/decoupe-finale.jpg) 
 
 
Finalement, après un petit coup de peinture, j'ai obtenu ceci : 
 
 
![](images/domino_peinture.jpg) 
![](images/domino_peinture2.jpg) 